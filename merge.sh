#!/bin/sh

# Check if the folder path is provided as an argument
if [ -z "$1" ]; then
  echo "Please provide the directory path containing the report files."
  exit 1
fi

# Merge individual reports into one
cd "$1" || exit 1
jq -s '.[0] * .[1] * .[2]' *.sshreport *.pmreport *.nwreport > report.json
echo "Report generated!"