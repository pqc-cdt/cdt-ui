function select_json_file() {
    let input = document.createElement("input");
    input.type = "file";
    input.onchange = handle_file_select;
    input.click();
}

function handle_file_select(event) {
    let files = event.target.files;
    let file_ext = files[0].name.split('.').pop();

    let table_pkg = document.getElementById("json-table-pkg");
    let table_ssh = document.getElementById("json-table-ssh");
    let table_nw = document.getElementById("json-table-nw");

    let reader = new FileReader();
    reader.readAsText(files[0]);
    reader.onload = function (event) {

        try {
            let json = event.target.result;
            let data = JSON.parse(json);

            populate_table(table_pkg, data);
            generate_charts(table_pkg, 'chart_pkgs', 'Severity', ['High', 'Medium', 'Low'], count_pkg_severities(table_pkg));

            populate_table(table_ssh, data);
            generate_charts(table_ssh, 'chart_ssh_keys', 'PQ-Secure', ['Secure', 'Insecure'], count_insecure_ssh_keys(table_ssh));

            var ssh_algos = count_ssh_algorithms_used(table_ssh);
            generate_charts(table_ssh, 'chart_ssh_keys_2', 'Algorithm', Object.keys(ssh_algos), Object.values(ssh_algos));

            populate_table(table_nw, data);

            new RetroNotify({
                contentHeader: 'Report',
                contentText: 'Successfully loaded report.',
                background: '#66BB6A',
                color: '#ffffff',
                openDelay: 0,
                closeDelay: 3000,
                animate: 'slideRightTop',
                contentClose: ''
            });
        } catch (error) {
            new RetroNotify({
                contentHeader: 'Report',
                contentText: 'Failed to load report.',
                background: '#FF6961',
                color: '#ffffff',
                openDelay: 0,
                closeDelay: 3000,
                animate: 'slideRightTop',
                contentClose: ''
            });
        }
    };
}


function count_pkg_severities(table) {
    var high_count = count_table_value_occurences(table, 'severity', 'high');
    var medium_count = count_table_value_occurences(table, 'severity', 'medium');
    var low_count = count_table_value_occurences(table, 'severity', 'low');

    return [high_count, medium_count, low_count];
}

function count_insecure_ssh_keys(table) {
    var secure_count = count_table_value_occurences(table, 'PQ-Secure', 'true');
    var insecure_count = count_table_value_occurences(table, 'PQ-Secure', 'false');

    return [secure_count, insecure_count];
}

function count_ssh_algorithms_used(table) {
    var occurrences = {};

    for (let i = 1; i < table.rows.length; i++) {
        var cell_value = table.rows[i].cells[2].textContent;

        if (occurrences.hasOwnProperty(cell_value)) {
            occurrences[cell_value]++;
        } else {
            occurrences[cell_value] = 1;
        }
    }

    return occurrences;
}


function count_table_value_occurences(table, columnName, searchString) {
    var columnIndex = -1;
    var rowCount = table.rows.length;
    var count = 0;

    // Find the index of the column matching the columnName parameter
    var headerRow = table.rows[0];
    for (var i = 0; i < headerRow.cells.length; i++) {
        if (headerRow.cells[i].textContent.trim().toLowerCase() === columnName.toLowerCase()) {
            columnIndex = i;
            break;
        }
    }

    if (columnIndex === -1) {
        console.error("Column not found: " + columnName);
        return count;
    }

    // Iterate over all rows and count the occurrences of the searchString parameter
    for (var i = 1; i < rowCount; i++) {
        var cell = table.rows[i].cells[columnIndex];
        if (cell.textContent.trim().toLowerCase() === searchString.toLowerCase()) {
            count++;
        }
    }

    return count;
}


function generate_charts(table, chart_id, chart_label, chart_label_data, table_data) {
    new Chart(document.getElementById(chart_id), {
        type: 'doughnut',
        data: {
            labels: chart_label_data,
            datasets: [{
                label: chart_label,
                data: table_data,
                borderWidth: 1
            }]
        }
    });
}

function search_pkg(table_id, input_id) {

    var input = document.getElementById(input_id);
    var table = document.getElementById(table_id);
    var filter = input.value.toLowerCase();
    var trs = table.querySelectorAll('tbody tr'), i;

    for (let i = 0; i < trs.length; ++i) {
        if (trs[i]) {
            let td = trs[i].cells[0];
            let txt_value = td.textContent || td.innerText;

            if (txt_value.toLowerCase().indexOf(filter) > -1) {
                trs[i].style.display = "";
            } else {
                trs[i].style.display = "none";
            }
        }
    }
}


function table_add_entry(table, ...values) {
    var body = table.getElementsByTagName('tbody')[0];

    let row = document.createElement("tr");

    for (let i = 0; i < values.length; i++) {
        let value = values[i];

        // Create a new cell for each value
        var cell = document.createElement('td');

        // Add icons to the table elements that require icons
        // column 3 = insecure package
        // column 4 = has insecure deps
        if (table.id == "json-table-pkg" && (i == 3 || i == 4)) {

            var icon = document.createElement('img');

            // If package is secure, or no insecure deps -> checkmark icon, else warn icon
            if (value == false) {
                icon.src = 'img/icon_checked.png';

            } else {
                icon.src = 'img/icon_warning.png';
            }
            icon.style.verticalAlign = 'middle';
            icon.style.width = '1em';
            icon.style.height = '1em';
            icon.style.marginRight = '0.5rem';
            cell.appendChild(icon);

            if (i == 3) {
                if (value == false) {
                cell.appendChild(document.createTextNode("Secure"));
            } else {
                cell.appendChild(document.createTextNode("Insecure"));
            }
            } else if (i == 4) {
                if (value == false) {
                    cell.appendChild(document.createTextNode("No"));
                } else {
                    cell.appendChild(document.createTextNode("Yes"));
                }
            } 
            
        } else {
            var text = document.createTextNode(value);
            cell.appendChild(text);
        }

        // Add the cell to the row
        row.appendChild(cell);
    }

    // Add the row to the table body
    body.appendChild(row);
}



function table_clear_rows(table) {
    var rowCount = table.rows.length;
    for (var i = rowCount - 1; i > 0; i--) {
        table.deleteRow(i);
    }
}

function populate_table(table, json_data) {

    // Make sure that table is cleared before new elements are added
    table_clear_rows(table);

    // Find which table to populate
    switch (table.id) {
        case "json-table-pkg": {
            let packages = json_data["packages"]
            // Only attempt to populate table if "packages" is not undefined
            if (packages != null) {
                for (let i = 0; i < packages.length; i++) {
                    // Package specific information
                    let package_name = packages[i]["name"];
                    let package_ver = packages[i]["version"];
                    let package_severity = packages[i]["severity"];
                    let is_insecure = packages[i]["is_insecure"];
                    let has_insecure_deps = packages[i]["has_insecure_deps"];
                    let insecure_deps_list = packages[i]["insecure_deps_list"];
                    table_add_entry(table, package_name, package_ver, package_severity, is_insecure, has_insecure_deps, insecure_deps_list);
                }

                // Set general informatin in the sidebar
                let scan_start = json_data["pm_scan_start"];
                let scan_end = json_data["pm_scan_end"];
                let sys_hostname = json_data["pm_sys_hostname"];
                let sys_kernel = json_data["pm_sys_kernel_version"];
                let sys_pkg_count = json_data["pm_sys_total_pkgs"];
                let sys_vuln_pkg_count = json_data["pm_sys_insecure_pkgs"];
                let sys_vuln_pkg_dep_count = json_data["pm_sys_insecure_pkg_deps"];
                document.getElementsByClassName("li-icon-scan-start")[0].childNodes[2].textContent = scan_start;
                document.getElementsByClassName("li-icon-scan-end")[0].childNodes[2].textContent = scan_end;
                document.getElementsByClassName("li-icon-host")[0].childNodes[2].textContent = sys_hostname;
                document.getElementsByClassName("li-icon-kernel")[0].childNodes[2].textContent = sys_kernel;
                document.getElementsByClassName("li-icon-total-packages")[0].childNodes[2].textContent = sys_pkg_count + " Packages";
                document.getElementsByClassName("li-icon-affected-packages")[0].childNodes[2].textContent = sys_vuln_pkg_count + " Packages";
                document.getElementsByClassName("li-icon-affected-packages-dep")[0].childNodes[2].textContent = sys_vuln_pkg_dep_count + " Packages";
            }
            break;
        }
        case "json-table-ssh": {
            let ssh_keys = json_data["ssh_keys"]
            for (let i = 0; i < ssh_keys.length; i++) {
                let file_path = ssh_keys[i]["file_path"];
                let owner = ssh_keys[i]["owner"];
                let algorithm = ssh_keys[i]["algorithm"];
                let key_size = ssh_keys[i]["key_size"];
                let pq_secure = ssh_keys[i]["pq_secure"];
                table_add_entry(table, file_path, owner, algorithm, key_size, pq_secure);
            }
            let key_count = json_data["ssh_key_count"];
            document.getElementsByClassName("li-icon-key-count")[0].childNodes[2].textContent = key_count + " SSH-Keys";

            break;
        }
        case "json-table-nw": {
            let traffic = json_data["traffic"]
            for (let i = 0; i < traffic.length; i++) {
                let protocol = traffic[i]["protocol"];
                let source = traffic[i]["source"];
                let destination = traffic[i]["destination"];
                let process = traffic[i]["process"];
                let timestamp = traffic[i]["timestamp"];
                let ciphersuite = traffic[i]["ciphersuite"];
                table_add_entry(table, protocol, source, destination, process, timestamp, ciphersuite);
            }

            // Set general informatin in the sidebar
            let traffic_count = json_data["nw_total_packet_count"];
            let traffic_count_identified = json_data["nw_identified_packet_count"];            
            document.getElementsByClassName("li-icon-total-packet-count")[0].childNodes[2].textContent = traffic_count + " Network Packets";
            document.getElementsByClassName("li-icon-identified-packet-count")[0].childNodes[2].textContent = traffic_count_identified + " Network Packets";
            break;
        }

    }
}