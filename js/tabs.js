function switch_tab(tab_name) {
   
    var tab_contents = document.getElementsByClassName("tab-content");
    for (let i = 0; i < tab_contents.length; i++) {
        tab_contents[i].style.display = "none";
    }

    var tabs = document.getElementsByClassName("tab");
    for (let i = 0; i < tabs.length; i++) {
        tabs[i].classList.remove("active");
    }

    document.querySelector(`.tab[data-tabname="${tab_name}"]`).classList.add("active");
    document.querySelector(`.tab-content[data-tabname="${tab_name}"]`).style.display = "flex";

}